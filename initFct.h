#ifndef INITFCT_H_INCLUDED
#define INITFCT_H_INCLUDED

#include <Eigen/Dense>

double sech(double const& x);

void damBreakInit(Eigen::ArrayXd & u0,Eigen::ArrayXd & h0, double & xMin, double & xMax, int const& nMaille, double & DeltaX, double & TFinal);

void solitonInit(Eigen::ArrayXd & u0, Eigen::ArrayXd & h0, double & xMin, double & xMax, int const& nMaille, double & DeltaX, double & TFinal);

void solitonExact(Eigen::ArrayXd & ue, Eigen::ArrayXd & he, double const& xMin, double const& xMax, double const& DeltaX, double const& tps);

void solitonDerH(Eigen::ArrayXd & dh, double const& xMin, double const& xMax, double const& DeltaX);

void solitonDoubleDerH(Eigen::ArrayXd & d2h, double const& xMin, double const& xMax, double const& DeltaX);

Eigen::ArrayXd XdSinh(Eigen::ArrayXd const& x);

Eigen::ArrayXd XdTanh(Eigen::ArrayXd const& x);

Eigen::ArrayXd XdSech(Eigen::ArrayXd const& x);

void solitonInitPerio(Eigen::ArrayXd & u0, Eigen::ArrayXd & h0, double & xMin, double & xMax, int const& nMaille, double & DeltaX, double & TFinal, double const& periode);

void solitonExactPerio(Eigen::ArrayXd & ue, Eigen::ArrayXd & he, double const& xMin, double const& xMax, double const& DeltaX, double const& tps, double const& periode);

#endif //INITFCT_H_INCLUDED
