/* Resolution des equations de Green Nagdhi
basee sur l'article: Le Metayer et al 2010, A numerical scheme for the GN model, jcp */

#include <iostream>
#include <fstream>
#include <cmath>

#include <Eigen/Dense>
#include <Eigen/Sparse>

#include "fonction.h"
#include "initFct.h"

using namespace std;
using namespace Eigen;

int main()
{
	cout<<"-------------------------------------------\n";
	cout<<"Résolution des equations de Green Naghdi 1D\n";
	cout<<"-------------------------------------------\n"<<endl;

	//constante
	double gravite(10.);  //gravite

	//Maillage (pas d'espace fixe)
	int nMaille(1000); // Nombre de maille
	int nPt(nMaille+1);

	double xMin(0.); 			// borne min du domaine
	double xMax(0.); 				// borne max du domaine
	double DeltaX(0.); 				// Pas d'espace

	//temps
	double TFinal(0.); 			// Temps final
	double DeltaT(0.); 				// Pas de temps, calcule CFL
	double tps(0.);					// Temps courant

    cout<<"Nombre de mailles: "<<nMaille<<", temps final: "<<TFinal<<" s\n"<<endl;

	// Conditions initiales
	ArrayXd vZero=ArrayXd::Zero(nPt);
	ArrayXd h0(vZero), u0(vZero);

	//dam break
	//damBreakInit(u0, h0, xMin, xMax, nMaille, DeltaX, TFinal);

	//soliton simple
	solitonInit(u0, h0, xMin, xMax, nMaille, DeltaX, TFinal);

	//soliton periodic
	//double periode(300.);
	//solitonInitPerio(u0, h0, xMin, xMax, nMaille, DeltaX, TFinal, periode);

	writeInFile(h0,u0,xMin,xMax,DeltaX,tps,"init");

	//initialisation des variables
	double unSurDeltaX(1./DeltaX);

	ArrayXd U1(vZero),U2(vZero),F1(vZero),F2(vZero); //U1=h, U2=hK, F1=hu, F2=hKu + gh^2/2 + alpha
	ArrayXd UNN1(vZero),UNN2(vZero); 	//U1,U2 au temps N+1
	ArrayXd K(vZero),alpha(vZero);
	ArrayXd a(nPt-2),b(nPt-2),c(nPt-2),d(vZero);
	ArrayXd hu(nPt-2);	     //systeme lineaire

	ArrayXd h = h0, u = u0;

	//verification soliton
	ArrayXd ue(vZero), he(vZero), udiff(vZero), hdiff(vZero), Ke(vZero), hKdiff(vZero);

	solitonExact(ue, he, xMin, xMax, DeltaX, tps);

	//solitonExactPerio(ue, he, xMin, xMax, DeltaX, tps, periode);

	writeInFile(he,ue,xMin,xMax,DeltaX,tps,"exact");

	MatrixXd mError=MatrixXd::Zero(int(TFinal),2);
	int ii=0;
	double testTps=0.;

	//boucle temps
	int nIteration(0);

	while (nIteration < 1 && tps < TFinal)
	{
        ArrayXd dh(vZero), d2h(vZero), alphaSol(vZero), KSol(vZero);

        solitonDerH(dh, xMin, xMax, DeltaX);
        solitonDoubleDerH(d2h, xMin, xMax, DeltaX);

        alphaSol=-2./3. * 100. * 121. * dh.square()/ h;
        KSol= u - 1./ (3. * h) *110.* ( 3. * dh.square() +  d2h * h - 2.*dh.square() ) ;


		calculK(K,u,h,DeltaX);
		calculAlpha(alpha,u,h,DeltaX);

        writeInFile(h, dh,xMin,xMax,DeltaX,tps,"derh");

        //K=KSol;
        //alpha=alphaSol;


		//K=u;
		//alpha=vZero;

		//calcul de (h)n+1 et (hK)n+1

		//U1
		U1 = h;
		//U2
		U2 = h*K;
		//F1
		F1 = h*u;
		//F2
		F2 = h*K*u + gravite*0.5*h.square() + alpha ;

		calculFluxHLL(UNN1,UNN2,DeltaT,
					u, h, U1, U2, F1, F2, gravite, DeltaX); //Flux et godunov

		tps += DeltaT;
		++nIteration;


        cout<<"\nIteration: "<<nIteration<<", Temps: "<<tps<<" s, DeltaT= "<< DeltaT <<" s\n";

		h=UNN1; 	//h au temps n+1
		//hu=UNN2;

		//verif
		solitonExact(ue, he, xMin, xMax, DeltaX, tps);
		calculK(Ke, ue, he, DeltaX);

        hdiff= h - he;
		hKdiff=he*Ke - UNN2;

		writeInFile(hdiff,hKdiff,xMin,xMax,DeltaX,tps,"hkdiff");

		//recherche (hu)n+1

		//neumann: reduction a une matrice tridiagonale

		//-Ci
        d = - 3. * UNN2 / ( h * h);
        ArrayXd temp;
        temp=d.segment(1, nPt-2);
        d=temp;


	    d(0)-=u(0) * h(0) * ( unSurDeltaX * unSurDeltaX -
			0.5 * unSurDeltaX * 0.5 / pow(h(0),2)* unSurDeltaX * ( pow(h(1),2) - pow(h(0),2) ) ) ;
		d.tail(1)-=u(nPt-1) * h(nPt-1) * ( unSurDeltaX * unSurDeltaX +
			0.5 * unSurDeltaX * 0.5 / pow(h(nPt-1),2) * unSurDeltaX * (pow(h(nPt-2),2) - pow(h(nPt-1),2) ))  ;

		calculDiagInf(a,h,unSurDeltaX);
		calculDiag(b,h,unSurDeltaX);
		calculDiagSup(c,h,unSurDeltaX);
		hu.resize(nPt-2);

		solveTridiago(hu, a, b, c, d);

		temp.resize(nPt);
        temp<<u(0)*h(0), hu, hu(hu.size()-1);
        hu=temp;

        //matrice pleine
		//MatrixXd A=MatrixXd::Zero(nPt-2,nPt-2);
		/*for (int i=0; i<nPt-2; ++i)
            A(i,i)=b(i);
        for (int i=0; i<nPt-3; ++i)
            {
                A(i+1,i)=a(i+1);
                A(i,i+1)=c(i);
            }
        VectorXd huTemp = A.colPivHouseholderQr().solve(d.matrix());
            */

        //matrice creuse
		/*SparseMatrix<double> A(nPt-2,nPt-2);

        A.reserve(VectorXi::Constant(nPt-2,3));
		for (int i=0; i<nPt-2; ++i)
            A.insert(i,i)=b(i);
        for (int i=0; i<nPt-3; ++i)
            {
                A.insert(i+1,i)=a(i+1);
                A.insert(i,i+1)=c(i);
            }
        A.makeCompressed();

        SparseQR< SparseMatrix<double>, COLAMDOrdering<int> > solver;
        solver.compute(A);
        Eigen::VectorXd huTemp = solver.solve(d.matrix());

        temp.resize(nPt);
        temp<<u(0)*h(0), huTemp, huTemp(huTemp.size()-1);
        hu=temp;*/

		//Calcul de u au temps n+1
        u=hu/h;

        if (tps-testTps>1.)
        {
            solitonExact(ue, he, xMin, xMax, DeltaX, tps);
            double rel_error;
            ArrayXd hhe=h-he, heh1=he-10.;
            rel_error= hhe.matrix().norm() / heh1.matrix().norm();

            mError(ii,0)=tps;
            mError(ii,1)=rel_error;
            testTps+=1.;
            ii++;
        }

        //if (nIteration % 30==0)
			//writeInFile(h,u,xMin,xMax,DeltaX,tps);
	}

    cout<<endl;

	writeInFile(h,u,xMin,xMax,DeltaX,tps,"end");

    solitonExact(ue, he, xMin, xMax, DeltaX, tps);
    writeInFile(he,ue,xMin,xMax,DeltaX,tps,"exact");

   // cout<<"\nErreur relative pour "<<nMaille<<" mailles\n  tps e"<<nMaille<<"\n"<<mError<<endl;

	cout<<"\nIterations: "<<nIteration<<", temps de fin:"<<tps<<" s\n";
	cout<<"Finito!!"<<endl;
	cout<<"----------------------------------------\n"<<endl;

	return 0;
}
