#include "fonction.h"

#include<iostream>
#include <iomanip>
#include<fstream>
#include<cmath>
#include<sstream>

using namespace std;
using namespace Eigen;

int writeInFile(ArrayXd const& h, ArrayXd const& u, double const& xMin, double const& xMax, double const& DeltaX, double const& tps, string const& file)
{
	stringstream NomFichier;
	NomFichier<<"data_"<<file.c_str()<<"_"<<tps<<".dat";

	ofstream fichier(NomFichier.str().c_str());

	if (fichier)
	{
		cout<<"Ecriture dans "<<NomFichier.str()<<endl;
		fichier<<"x h"<<file.c_str()<<" u"<<file.c_str()<<endl;
		for (int i=0; i<h.size(); ++i)
		{
			fichier<<std::fixed;
			fichier<<std::setprecision(9) <<DeltaX * i + xMin <<" "<<h(i)<<" "<<u(i)<<endl;
		}

		if (abs (DeltaX * (h.size()-1.) + xMin - xMax) > 0.1 * DeltaX)
			cout<<"oulala!! xMax pas atteint "<<DeltaX * (h.size()-1.) + xMin<< " xMax "<<xMax <<endl;

		fichier.close();

		return 0;
	}
	else
	{
		cout << "ERREUR: Impossible d'ouvrir le fichier au temps "<< tps<< " s." << endl;
		return 1;
	}
}

void calculFluxHLL(	ArrayXd & UNN1,   ArrayXd & UNN2, double & DeltaT,
					ArrayXd const& u, ArrayXd const& h,
					ArrayXd const& U1,ArrayXd const& U2,
					ArrayXd const& F1,ArrayXd const& F2,
					double const& gravite, double const& DeltaX)
//calcul du flux avec solver de riemann HLL
{
    int taille(u.size());
	ArrayXd sigL(taille-1),sigR(taille-1);
	ArrayXd FHLL1(taille-1),FHLL2(taille-1);
	double maxSigma(u(0) + sqrt(gravite * h(0)));

	for(int i=0; i < taille -1 ; ++i)
	{
		//wave speed
		/*sigL(i) = u(i)   - sqrt(gravite*h(i));
		sigR(i) = u(i+1) + sqrt(gravite*h(i+1));*/


        sigL(i) = min( u(i) - sqrt(gravite*h(i)), u(i+1) - sqrt(gravite*h(i+1) ));
		sigR(i) = max( u(i) + sqrt(gravite*h(i)), u(i+1) + sqrt(gravite*h(i+1) ));


		//max wave speed
		maxSigma=max(max(abs(sigL(i)), abs(sigR(i)) ) , maxSigma);

		//flux
		/*FHLL1(i)=( sigR(i) * F1(i) - sigL(i) * F1(i+1) -
					sigR(i) * sigL(i) * (U1(i) - U1(i+1))) / (sigR(i) -sigL(i));
        FHLL2(i)=( sigR(i) * F2(i) - sigL(i) * F2(i+1) -
					sigR(i) * sigL(i) * (U2(i) - U2(i+1))) / (sigR(i) -sigL(i));*/

		/*if (sigL(i)>=0)
			{
			FHLL1(i)=F1(i);
			FHLL2(i)=F2(i);
			}
		else
			if (sigR(i)<=0)
				{
				FHLL1(i)=F1(i+1);
				FHLL2(i)=F2(i+1);
				}
			else
				{*/
				FHLL1(i)=( sigR(i) * F1(i) - sigL(i) * F1(i+1) -
					sigR(i) * sigL(i) * (U1(i) - U1(i+1))) / (sigR(i)-sigL(i));
				FHLL2(i)=( sigR(i) * F2(i) - sigL(i) * F2(i+1) -
					sigR(i) * sigL(i) * (U2(i) - U2(i+1))) / (sigR(i) -sigL(i));
				//}
	}

	//determiner DeltaT
	double CFL(0.8);
	double unSurDeltaX(1./DeltaX);

	DeltaT = CFL * DeltaX / maxSigma;


	//Godunov
	//Ui,n+1= Ui,n - DeltaT / DeltaX *(Fi+1/2 -Fi-I/2)

	//premier point
	//UNN1(0)= h(0);
	//UNN2(0)= h(0)*u(0);
	UNN1(0)=U1(0);
    UNN2(0)=U2(0);
	//condition symetrique
	//UNN1.at(0)=( U1.front() - DeltaT * unSurDeltaX * (FHLL1.front() - FHLL1.back()));
	//UNN2.at(0)=( U2.front() - DeltaT * unSurDeltaX * (FHLL2.front() - FHLL2.back()));

	for (size_t i=1; i < taille-1; ++i)
	{
		UNN1(i)= U1(i) - DeltaT * unSurDeltaX * (FHLL1(i) - FHLL1(i-1));
		UNN2(i)= U2(i) - DeltaT * unSurDeltaX * (FHLL2(i) - FHLL2(i-1));
	}

	//neuman
	//UNN1(0)=UNN1(1);
	//UNN2(0)=UNN2(1);
	UNN1(taille-1)=U1(taille-1);
	UNN2(taille-1)=U2(taille-1);
	//UNN1(taille-1)=UNN1(taille-2);
	//UNN2(taille-1)=UNN2(taille-2);

	//symetrique
	//UNN1.push_back( U1.back() - DeltaT * unSurDeltaX * (FHLL1.front() - FHLL1.back()));
	//UNN2.push_back( U2.back() - DeltaT * unSurDeltaX * (FHLL2.front() - FHLL2.back()));
}

void deriveSchemaCentre(ArrayXd & df, ArrayXd const& f, double const& Delta)
//derive simple, schema centre
{
	double unSurDelta(1./Delta);
	int taille(f.size());

	//neuman
	df(0) = unSurDelta * (f(1)-f(0));

	//symetrique
	//df(0)= 0.5 * unSurDelta * (f(1)-f(taille-2));

	for (int i = 1; i < taille-1; ++i)
		df(i) = 0.5 * unSurDelta * (f(i+1) - f(i-1));

	//neumann
	df(taille-1) = unSurDelta * (f(taille-1) - f(taille-2));

	//symetrique
	//df(taille-1) = 0.5 * unSurDelta * (f(1) - f(taille-2));
}

void deriveDoubleCentre(ArrayXd& d2f, ArrayXd const& f, double const& Delta)
//derive double, schema centre
{
	double unSurDeltaCarre(1./(Delta * Delta));
	int taille(f.size());

	//Neumann
	d2f(0) =  0.5 * unSurDeltaCarre * ( f(2) + f(0) - 2.*f(1)) ;

	//symetrique
	//d2f(0)=unSurDeltaCarre * (f(1) + f(taille-2) - 2. * f(0));

	for (int i=1; i<taille-1; ++i)
		d2f(i) = unSurDeltaCarre * (f(i+1) + f(i-1) - 2. * f(i));

	//Neumann
	d2f(taille-1)= 0.5 * unSurDeltaCarre * (f(taille-1) + f(taille-3) - 2. * f(taille-2));

	//symetrique
	//d2f(taille-1)= unSurDeltaCarre * (f(1) + f(taille-2) - 2. * f(taille-1));
}

void calculK(ArrayXd & K, ArrayXd const& u, ArrayXd const& h,double const& DeltaX)
{
    int taille(u.size());
    ArrayXd dhu(taille),dh2(taille),d2h2(taille),d2hu(taille);

	ArrayXd h2,hu;

    h2=h*h;
    hu=h*u;

	deriveSchemaCentre(dh2, h2, DeltaX);
	deriveSchemaCentre(dhu, hu, DeltaX);
	deriveDoubleCentre(d2h2,h2,DeltaX);
	deriveDoubleCentre(d2hu,hu,DeltaX);

    K= u - h * d2hu / 3. + u * d2h2 / 6. - dh2 * dhu / ( 6. * h);

    ArrayXd Kbis,h3,du(taille),dh3du(taille);

    h3=h.cube();
    deriveSchemaCentre(du,u,DeltaX);
    deriveSchemaCentre(dh3du,h3*du,DeltaX);

    Kbis=u - dh3du / (3.*h);
    cout<<"\nNormK-Kbis "<<(K-Kbis).matrix().norm()<<endl;

    writeInFile(K,Kbis,-500.,500,DeltaX,0,"KKbis");


    //K=Kbis;
}


void calculAlpha(ArrayXd & alpha, ArrayXd const& u, ArrayXd const& h,double const& DeltaX)
{
    int taille(u.size());
	/*ArrayXd dhu(taille),dh2(taille);
	ArrayXd h2,hu;

    h2=h*h;
    hu=h*u;

	deriveSchemaCentre(dh2, h2, DeltaX);
	deriveSchemaCentre(dhu, hu, DeltaX);

	alpha=-2./(3.* h) *  pow(( h * dhu - 0.5 * u * dh2),2);*/

	ArrayXd alphabis(taille), du(taille);

	deriveSchemaCentre(du, u, DeltaX);

	alphabis=-2./3. * h.cube() * du.square();
    //cout<<"\nNorm "<<(alpha-alphabis).matrix().norm()<<endl;

    alpha=alphabis;
}

void solveTridiago(ArrayXd & X, ArrayXd const& a, ArrayXd const& b, ArrayXd c, ArrayXd const& d)
/* matrice tridiagonal.
*  AX=d, A: diago inf a, diago  b, diago sup c. resultat dans X
    |b0 c0 0 ||x0| |d0|
    |a1 b1 c1||x1|=|d1|
    |0  a2 b2||x2| |d2|
*/
{
	size_t N(a.size());

	c(0) /= b(0);
	X=d;
    X(0)/=b(0);

    for (size_t i = 1; i < N-1; ++i)
	{
        c(i) /= b(i) - a(i)*c(i-1);
        X(i) = (X(i) - a(i)*X(i-1)) / (b(i) - a(i)*c(i-1));
    }

    X(N-1) = (X(N-1) - a(N-1)*X(N-2)) / (b(N-1) - a(N-1)*c(N-2));

    for (int i = N-2; i >= 0; --i)
	{
        X(i) -= c(i) * X(i+1);
    }

	/*	vector<double> a(4,-1.),b(4,4.),c(4,-1),d(4,5.);
	vector<double> hu;	//systeme lineaire
	vector<double> temp,temp2;

	a.front()= 0.;
    c.back() = 0 ;
    d.at(2) =10; d.back()= 23;
	solveTridiago(hu, a, b, c, d);

	cout<<"res ";
	for(size_t i=0; i<hu.size(); ++i)
		cout<<" "<<hu.at(i);
	cout<<endl;
	cout<<"Correct: 2 3 5 7"<<endl;

	a.clear();
	b.clear();
	c.clear();
	d.clear();
	hu.clear();

	a.push_back(0); a.push_back(2);	a.push_back(5);
	b.push_back(1);	b.push_back(4); b.push_back(7);
	c.push_back(3);	c.push_back(6); c.push_back(0);
	d.push_back(1);	d.push_back(2); d.push_back(3);

	solveTridiago(hu,a, b,c,d);

	cout<<"res ";
	for(size_t i=0; i<hu.size(); ++i)
		cout<<" "<<hu.at(i) * 22.<<"/22";
	cout<<endl;
	cout<<"Correct: -5/22 9/22 3/22"<<endl;*/
}


void calculDiagInf(ArrayXd & a, ArrayXd const& h, double const& unSurDeltaX)
{
	size_t n(h.size()-1);
	ArrayXd dh2(n+1),h2;

    h2=h*h;
    deriveSchemaCentre(dh2, h2, 1./unSurDeltaX);

	a(0)=0.;

	//neumann
	for(size_t i=2; i<n; ++i)
		a(i-1)= unSurDeltaX * unSurDeltaX - 0.5 * 0.5 * unSurDeltaX *  dh2(i) / h2(i);

	//symetrique
	//for(size_t i=1; i<=n; ++i)
		//a(i)=unSurDeltaX * unSurDeltaX - 0.5 * unSurDeltaX * 0.5 * dh2(i) / h2(i);
}

void calculDiagSup(ArrayXd & c, ArrayXd const& h, double const& unSurDeltaX)
{
	size_t n(h.size()-1);
	ArrayXd dh2(n+1),h2;

    h2=h*h;
    deriveSchemaCentre(dh2, h2, 1./unSurDeltaX);

	//neumann
	for(size_t i=1; i<n-1; ++i)
		c(i-1)=unSurDeltaX * unSurDeltaX + 0.5 * 0.5 * unSurDeltaX *  dh2(i) / h2(i);

	//symetrique
	//for(size_t i=0; i<=n-1; ++i)
		//c(i)=unSurDeltaX * unSurDeltaX + 0.5 * unSurDeltaX * 0.5 * dh2(i) / h2(i);

	c(c.size()-1)=0.;
}

void calculDiag(ArrayXd & b, ArrayXd const& h, double const& unSurDeltaX)
{
	size_t n(h.size()-1);
	ArrayXd d2h2(n+1),h2;

    h2=h*h;
	deriveDoubleCentre(d2h2,h2,1./unSurDeltaX);

	//neumann
	for(size_t i=1; i<n; ++i)
		b(i-1)=-0.5 * ( 6. + d2h2(i) ) / h2(i) - 2. * unSurDeltaX * unSurDeltaX ;

	//symetrique
	//for(size_t i=0; i<=n; ++i)
		//b(i)=-0.5 * ( 6. + d2h2(i) ) / h2(i) - 2. * unSurDeltaX * unSurDeltaX ;
}

//void transfoTridiago(vector<double> & a, vector<double> & b, vector<double> & c, vector<double> & d)
/* 	transformation d'une systeme lineaire presque tridiago (M_1,N) et (M_N,1) non nul en matrice tridiago
	a diagonale inferieur, b milieu, c supperieur
	|b1 c1  0  a1||x0| |d0|
    |a2 b2 c2  0 ||x1|=|d1|
     0
	|cn 0   an bn||x2| |d2|
*/
/*{
	double p(0.), p1(0.), p2(0.), g(0,), g1(0.), g2(0.);
	int N(a.size());

	p1=c.back();
	g1=a.front();
	for(int i=2; i<N-2; ++i)
	{
		//ln'=p1/a[i] * li - ln, utilisation de la ligne i pour supprimer le coefficient a la ligne n
		p = p1 / a.at(i);
		p1= p  * b.at(i) - p2;
		p2= p  * c.at(i);
		d.back()= p * d.at(i) - d.back();

		// pour la premiere ligne
		//l1'=g1/c[i] * li - l1
		g = g1 / c.at(N-i);
		g1= g  * b.at(N-i) - g2;
		g2= g  * a.at(N-i);
		d.front() = g * d.at(N-i) - d.front();
	}

	//ligne N-2 -ligne N
	p = p1 / a.at(N-2);
	p1= p  * b.at(N-2) - p2;
	a.back()= p * c.at(N-2) - a.back();
	d.back()= p * d.at(N-2) - d.back();

	//ligne N-1-ligne N
	p = p1 / a.at(N-1);
	a.back()= p * b.at(N-1) - a.back();
	b.back()= p * c.at(N-1) - b.back();
	d.back()= p * d.at(N-1) - d.back();

	//ligne 3 -ligne 1
	g = g1 / a.at(3);
	g1= g  * b.at(3) - g2;
	a.front()= g * c.at(3) - a.front();
	d.front()= g * d.at(3) - d.front();

	//ligne 2-ligne 1
	p = g1 / a.at(2);
	a.front()= g * b.at(2) - a.front();
	b.front()= g * c.at(2) - b.front();
	d.front()= g * d.at(2) - d.front();

}*/
