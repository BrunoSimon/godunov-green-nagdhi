#include "initFct.h"

#include<iostream>
#include<cmath>

using namespace std;
using namespace Eigen;

double sech(double const& x)
{
	return 2./(exp(x)+exp(-x));
}

void damBreakInit(ArrayXd & u0, ArrayXd & h0, double & xMin, double & xMax, int const& nMaille, double & DeltaX, double & TFinal)
{
    double hDam(1.8), hLow(1.), xDam(0.);
    xMin=-300.;
    xMax=300.;
    double lDomaine(xMax-xMin); 	// Longueur du domaine
    DeltaX=lDomaine/double(nMaille);
    TFinal=48. ;

	for (int i = 0; i * DeltaX + xMin <= xMax + 0.5 * DeltaX; ++i)
		{
		if (i * DeltaX + xMin < xDam)
			h0(i)=hDam;
		else
			h0(i)=hLow;

		u0(i)=0.;
		}
}

void solitonInit(ArrayXd & u0, ArrayXd & h0, double & xMin, double & xMax, int const& nMaille, double & DeltaX, double & TFinal)
{
    double h1(10.), h2(12.1), D(11.); //test case neuman
    xMin=-500.;
    xMax=500.;
    TFinal= 12. ;
    double lDomaine(xMax-xMin); 	// Longueur du domaine
    DeltaX=lDomaine/double(nMaille);

	double x(xMin);

	h0=ArrayXd::Constant(h0.rows(),0.);
	u0=h0;
	int i=0;

	while ( x < xMax + 0.5 * DeltaX )
		{
		h0(i)=( h1 + (h2 - h1) * pow ( sech ( 0.5 * x * sqrt( 3. * (h2 - h1) / (h2*h1*h1) ) ), 2. ) ) ;
		u0(i)=(D * (1. - h1/( h0(i) ) ) );

		x += DeltaX;
		i++;
		}

}

void solitonExact(ArrayXd & ue, ArrayXd & he, double const& xMin, double const& xMax, double const& DeltaX, double const& tps)
{
    double h1(10.), h2(12.1), D(11.);
	he=ArrayXd::Constant(he.rows(),0.);
	ue=he;
	double x(xMin);

    int i=0;
	while ( x < xMax + 0.5 * DeltaX )
		{
		he(i)=( h1 + (h2 - h1) * pow ( sech ( 0.5 * (x-D*tps) * sqrt( 3. * (h2 - h1) / (h2*h1*h1) ) ), 2. ) ) ;
		ue(i)=(D * (1. - h1/( he(i) ) ) );

		x += DeltaX;
		i++;
		}
}

void solitonDerH(ArrayXd & dh, double const& xMin, double const& xMax, double const& DeltaX)
{
    double h1(10.), h2(12.1), D(11.);
	/*he=ArrayXd::Constant(he.rows(),0.);
	ue=he;*/
	int taille;
	taille= int((xMax - xMin) / DeltaX + DeltaX*0.5) +1;

	ArrayXd x(taille), xA(taille);

	for (int i=0; i<taille; ++i)
        x(i)=xMin + i * DeltaX;

    double A=sqrt(3.*(h2-h1)/(h2*h1*h1));

    xA=0.5 * A * x ;

    dh= -(h2-h1) * A * XdSinh(xA) * XdSech(xA).cube();
}

void solitonDoubleDerH(ArrayXd & d2h, double const& xMin, double const& xMax, double const& DeltaX)
{
    double h1(10.), h2(12.1), D(11.);
	/*he=ArrayXd::Constant(he.rows(),0.);
	ue=he;*/
	int taille;
	taille= int((xMax - xMin) / DeltaX + DeltaX*0.5) +1;

	ArrayXd x(taille), xA(taille);

	for (int i=0; i<taille; ++i)
        x(i)=xMin + i * DeltaX;

    double A=sqrt(3.*(h2-h1)/(h2*h1*h1));

    xA=0.5 * A * x ;

    d2h= -1.5 * pow(h2-h1, 2.) / (h2*h1*h1) * (1. - 3.* XdTanh(xA).square()) * XdSech(xA).square() ;
}

ArrayXd XdSinh( ArrayXd const& x)
// 0.5 *(exp(x)-exp(-x))
{
    ArrayXd res(x), minusx(-x);

    res= 0.5 * ( x.exp() - minusx.exp() );

    return res;
}

ArrayXd XdTanh( ArrayXd const& x)
// tanh(x) = sinh(x)/cosh(x) = 1 - 2/(exp(2x)+1)
{
    ArrayXd res(x), deuxX(2*x);

    res= 1. - 2. / ( deuxX.exp() + 1.);

    return res;
}

ArrayXd XdSech( ArrayXd const& x)
// sesh(x) = 2 / (exp(x) + exp(-x))
{
    ArrayXd res(x), minusx(-x);

    res= 2. / ( x.exp() + minusx.exp() );

    return res;
}


/*void KExactSoliton(ArrayXd & Ke, double const& xMin, double const& xMax, double const& DeltaX)
{
    double h1(10.), h2(12.1), D(11.);
	/*he=ArrayXd::Constant(he.rows(),0.);
	ue=he;*/
	/*double x(xMin);


}
*/

void solitonInitPerio(ArrayXd & u0, ArrayXd & h0, double & xMin, double & xMax, int const& nMaille, double & DeltaX, double & TFinal, double const& periode)
{
    double h1(10.), h2(22.5), D(15.); //test case periodic

    xMin=0.;
    xMax=300.;
    TFinal=80. ;
    double lDomaine(xMax-xMin); 	// Longueur du domaine
    DeltaX=lDomaine/double(nMaille);

	double x(xMin);

	h0=ArrayXd::Constant(h0.rows(),0.);
	u0=h0;
	int i=0;

	while ( x < xMax +  0.5 * DeltaX )
    {
        if (x<= xMin + periode * 0.5 +  0.5 * DeltaX)
        {
            h0(i)=( h1 + (h2 - h1) * pow ( sech ( 0.5 * x * sqrt( 3. * (h2 - h1) / (h2*h1*h1) ) ), 2. ) ) ;
            u0(i)=(D * (1. - h1/( h0(i) ) ) );
        }
        else
        {
            h0(i)=h0(nMaille-i);
            u0(i)=u0(nMaille-i);
        }

		x += DeltaX;
		i++;
	}

}

void solitonExactPerio(ArrayXd & ue, ArrayXd & he, double const& xMin, double const& xMax, double const& DeltaX, double const& tps, double const& periode)
{
    double h1(10.), h2(22.5), D(15.); //test case periodic
	he=ArrayXd::Constant(he.rows(),0.);
	ue=he;
	double x(xMin);

    int i=0;
	while ( x < xMax + 0.5 * DeltaX )
		{
		he(i)=( h1 + (h2 - h1) * pow ( sech ( 0.5 * (x-D*tps) * sqrt( 3. * (h2 - h1) / (h2*h1*h1) ) ), 2. ) ) ;
		ue(i)=(D * (1. - h1/( he(i) ) ) );

		x += DeltaX;
		i++;
		}
}

