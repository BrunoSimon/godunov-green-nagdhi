/* Resolution des equations de Green Nagdhi
basee sur l'article: Le Metayer et al 2010, A numerical scheme for the GN model, jcp */

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

#include "fonction.h"
#include "initFct.h"

using namespace std;

int main()
{
	cout<<"-------------------------------------------\n";
	cout<<"Résolution des equations de Green Naghdi 1D\n";
	cout<<"-------------------------------------------\n"<<endl;

	//constante
	double gravite(10.);  //gravite

	//Maillage (pas d'espace fixe)
	double xMin(-500.); 			// borne min du domaine
	double xMax(500.); 				// borne max du domaine
	double lDomaine(xMax-xMin); 	// Longueur du domaine
	double DeltaX(0.5); 				// Pas d'espace
	double unSurDeltaX(1./DeltaX);
	int nMaille(round(lDomaine/DeltaX)); // Nombre de maille
	
	//temps
	double TFinal(48.); 			// Temps final
	double DeltaT(0.); 				// Pas de temps, calcule CFL
	double tps(0.);					// Temps courant

	
	// Conditions initiales
	vector<double> h0,u0;
	
	//dam break
	double hDam(1.8), hLow(1.), xDam(0.);
	damBreakInit( u0, h0,hDam, hLow, xMin, xMax, xDam, DeltaX);

	
	//soliton
	//double h1(10.), h2(12.1), D(11.);
	//solitonInit(u0, h0, h1, h2, D, xMin, xMax, DeltaX);
		
	
	
	writeInFile(h0,u0,xMin,xMax,DeltaX,tps);
	
	//initialisation des variables
	vector<double> U1,U2,F1,F2; //U1=h, U2=hK, F1=hu, F2=hKu + gh^2/2 + alpha
	vector<double> UNN1,UNN2; 	//U1,U2 au temps N+1
	vector<double> K,alpha;		
	vector<double> a,b,c,d;
	vector<double> hu;	//systeme lineaire
	vector<double> temp,temp2;
	
	vector<double> h(h0), u(u0);
	
	//verification solition
	/*vector<double> ue, he, udiff, hdiff, Ke, hKdiff;
	
	solitonExact(ue, he, h1, h2,  D, xMin, xMax, DeltaX, tps);
	writeInFile(he,ue,xMin,xMax,DeltaX,tps+100.);*/

	
	//boucle temps
	int nIteration(0);
	while (nIteration<50000 && tps < TFinal)
	{
		cout<<endl<<"Iteration: "<<nIteration<<", Temps: "<<tps<<endl;

		calculK(K,u,h,DeltaX);
		calculAlpha(alpha,u,h,DeltaX);
		
		//K=u;
		//multiCstVecteur(alpha,u,0.); //=0
		
		//calcul de (h)n+1 et (hK)n+1
				
		//U1
		U1=h; //h
		//U2
		produitVecteurCompo(U2,h,K); //hk
		//F1
		produitVecteurCompo(F1,h,u); //hu
		//F2
		produitVecteurCompo(temp,h,h);  // h^2
		multiCstVecteur(temp2,temp,gravite * 0.5);  //  h^2*g/2
		temp.clear();
		sommeVecteur(temp,alpha,temp2); //  h^2*g/2 + alpha
		temp2.clear();
		produitVecteurCompo(temp2,F1,K); // hKu
		sommeVecteur(F2,temp,temp2); //hKu + h^2*g/2 + alpha
		
		
		temp.clear();
		temp2.clear();
		
		calculFluxHLL(UNN1,UNN2,DeltaT, 
					u, h, U1, U2, F1, F2, gravite, DeltaX); //Flux et godunov
		
		cout<<"DeltaT= "<< DeltaT <<" s"<<endl;
		
		tps += DeltaT;
		++nIteration;
	
		h=UNN1; 	//h au temps n+1
		//hu=UNN2;

		/*ue.clear();
		he.clear();
		Ke.clear();
		hdiff.clear();
		hKdiff.clear();
		
		solitonExact(ue, he, h1, h2,  D, xMin, xMax, DeltaX, tps);
		calculK(Ke,ue,he,DeltaX);
		
		multiCstVecteur(temp,he,-1.);	// -hexact
		sommeVecteur(hdiff,h,temp); 	// hn+1-hexact
		temp.clear();
		
		produitVecteurCompo(temp2,he,Ke); //hkexact
		multiCstVecteur(temp,temp2,-1.);	// -hKexact
		sommeVecteur(hKdiff,UNN2,temp); 	// hKn+1-hKexact
		temp.clear();
		temp2.clear();
	
		writeInFile(hdiff,hKdiff,xMin,xMax,DeltaX,tps+700.);*/
		
		//recherche (hu)n+1
		
		
		//neumann: reduction a une matrice tridiagonale
		a.clear(); //diag inf
		b.clear(); //diag
		c.clear(); //diag sup
		d.clear();
		hu.clear();
		
		
		//-Ci
		produitVecteurCompo(temp,h,h); 			//h^2
		divisionVecteurCompo(temp2,UNN2,temp); 	//(hK)_i/h_i^2
		multiCstVecteur(d,temp2,-3.); 			//-3*(hK)_i/h_i^2
		
		d.erase(d.begin());
		d.erase(d.end()-1);
		
		d.front()-=u.front() * h.front() * ( unSurDeltaX * unSurDeltaX - 
			0.5 * unSurDeltaX * 0.5 / pow(h.front(),2)* unSurDeltaX * ( pow(h.at(1),2) - pow(h.at(0),2) )   ) ;
		d.back()-=u.back() * h.back() * ( unSurDeltaX * unSurDeltaX + 
			0.5 * unSurDeltaX * 0.5 / pow(h.back(),2) * unSurDeltaX * ( pow(h.at(h.size()-2),2) - pow(h.back(),2) ) ) ;
		
		
		temp.clear();
		temp2.clear();
		
		calculDiagInf(a,h,unSurDeltaX);
		calculDiag(b,h,unSurDeltaX);
		calculDiagSup(c,h,unSurDeltaX);
		
		solveTridiago(hu, a, b, c, d);
		
		hu.insert(hu.begin(),u.at(0)*h.at(0));
		hu.push_back(hu.back());
		
		//fin neumann et tridiago
		
		u.clear();
		
		divisionVecteurCompo(u,hu,h); //u au temps n+1
		
		/*udiff.clear();
		temp.clear();
		multiCstVecteur(temp,ue,-1.);	// -uexact
		sommeVecteur(udiff,u,temp); 	// un+1-uexact
		temp.clear();
		
		writeInFile(hdiff,udiff,xMin,xMax,DeltaX,tps+500. );*/
		
		K.clear();
		alpha.clear();
		F1.clear();
		F2.clear();
		U1.clear();
		U2.clear();
		UNN1.clear();
		UNN2.clear();
				
		//if (nIteration % 30==0)	
			//writeInFile(h,u,xMin,xMax,DeltaX,tps);
	}
	
	writeInFile(h,u,xMin,xMax,DeltaX,tps);
	
	/*solitonExact(ue, he, h1, h2,  D, xMin, xMax, DeltaX, tps);
	writeInFile(he,ue,xMin,xMax,DeltaX,tps + 100.);*/
	
	cout<<"Iterations: "<<nIteration<<", mailles: "<<nMaille<<endl;
	cout<<"Finito!!"<<endl;
	cout<<"----------------------------------------\n"<<endl;
	return 0;
}
