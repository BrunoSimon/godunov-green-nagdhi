#ifndef FONCTION_H_INCLUDED
#define FONCTION_H_INCLUDED

#include <string>
#include <Eigen/Dense>

int writeInFile(Eigen::ArrayXd const& h, Eigen::ArrayXd const& u, double const& xMin, double const& xMax, double const& DeltaX, double const& tps, std::string const& file);

void deriveSchemaCentre(Eigen::ArrayXd & df, Eigen::ArrayXd const& f, double const& Delta);

void deriveDoubleCentre(Eigen::ArrayXd& d2f, Eigen::ArrayXd const& f, double const& Delta);

void calculK(Eigen::ArrayXd & K, Eigen::ArrayXd const& u, Eigen::ArrayXd const& h,double const& DeltaX);

void calculAlpha(Eigen::ArrayXd & alpha, Eigen::ArrayXd const& u, Eigen::ArrayXd const& h,double const& DeltaX);

void calculFluxHLL(	Eigen::ArrayXd  & UNN1,   Eigen::ArrayXd  & UNN2, double & DeltaT,
					Eigen::ArrayXd  const& u, Eigen::ArrayXd  const& h,
					Eigen::ArrayXd  const& U1,Eigen::ArrayXd  const& U2,
					Eigen::ArrayXd  const& F1,Eigen::ArrayXd  const& F2,
					double const& gravite, double const& DeltaX);

void solveTridiago(Eigen::ArrayXd & X, Eigen::ArrayXd const& a, Eigen::ArrayXd const& b, Eigen::ArrayXd c, Eigen::ArrayXd const& d);

void calculDiag(Eigen::ArrayXd & b, Eigen::ArrayXd const& h, double const& unSurDeltaX);

void calculDiagInf(Eigen::ArrayXd & a, Eigen::ArrayXd const& h, double const& unSurDeltaX);

void calculDiagSup(Eigen::ArrayXd & c, Eigen::ArrayXd const& h, double const& unSurDeltaX);

//void transfoTridiago(std::vector<double> & a, std::vector<double> & b, std::vector<double> & c);

#endif //FONCTION_H_INCLUDED
